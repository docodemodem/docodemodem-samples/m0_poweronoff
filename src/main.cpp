#include <docodemo.h>
#include <SlrModem.h>

#define DEBUGPRNT UartExternal // USBは使えないで外部UARTをデバッグ用にします。

const uint8_t CHANNEL = 0x10;   // 10進で16チャネルです。通信相手と同じ値にしてください。
const uint8_t DEVICE_DI = 0x00; // 通信相手のIDです。0は全てに届きます。
const uint8_t DEVICE_EI = 2;    // 自分のIDです
const uint8_t DEVICE_GI = 0x02; // グループIDです。通信相手と同じ値にしてください。

DOCODEMO Dm = DOCODEMO();
SlrModem modem;

volatile bool alarmTriggered = false;

void sendVolt()
{
  // モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  // modem uart
  UartModem.begin(MLR_BAUDRATE);

  // モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  if (!digitalRead(POW_STA))
  {
    // 各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存すると毎回設定は不要です。
    // 今回は強制電源ONの時に保存して、RTC起動時は時短のために設定しないようにします。
    modem.SetMode(SlrModemMode::LoRaCmd, true);
    modem.SetChannel(CHANNEL, true);
    modem.SetDestinationID(DEVICE_DI, true);
    modem.SetEquipmentID(DEVICE_EI, true);
    modem.SetGroupID(DEVICE_GI, true);
  }

  float val = Dm.readExADC(); // AD値を読みます。
  // val *= 10.0;  //4-20mAの場合のmA変換。100オームでAをVに変換しているので10倍するとmAになる。

  char data[32];
  int size = snprintf(data, sizeof(data),"ADC %.2f", val);
  DEBUGPRNT.println(data);

  auto rc = modem.TransmitData((uint8_t *)data, size);
  if (rc == SlrModemError::Ok)
  {
    // 送信完了
    DEBUGPRNT.println("Send Ok");
  }
  else
  {
    // キャリアセンスによって送信できなかったことを示します
    DEBUGPRNT.println("Send Ng...");
  }
}

void wakeup(void)
{
  alarmTriggered = true;
}

#define LOOP_COUNT 1

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  Dm.begin();

  Dm.exPowerCtrl(ON);

  DateTime now = Dm.rtcNow();
  char buf[64];
  snprintf(buf, sizeof(buf), "now %d/%02d/%02d %02d:%02d:%02d", now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second());
  DEBUGPRNT.println(buf);

  if (digitalRead(POW_STA))
  {
    // SW1がLOW。RTCによる電源断からの立ち上げ
    Dm.LedCtrl(GREEN_LED, OFF);
    Dm.LedCtrl(RED_LED, ON);

    sendVolt();

    DEBUGPRNT.flush();
    
    Dm.ModemPowerCtrl(OFF);
    Dm.exPowerCtrl(OFF);
    Dm.LedCtrl(GREEN_LED, OFF);
    Dm.LedCtrl(RED_LED, OFF);

    // Power off
    pinMode(MAIN_PWR_CNT_OUT, OUTPUT); // ここで電源が切れます
  }
  else
  {
    // SW1がHIGH。強制電源ONで立ち上げ
    Dm.LedCtrl(GREEN_LED, ON);
    Dm.LedCtrl(RED_LED, OFF);

    // set wakeup timer
    uint16_t sec = LOOP_COUNT * 60;
    Dm.setRtcTimer(RtcTimerPeriod::SECONDS, sec, wakeup);

    sendVolt(); // for test shot

    while (1)
    {
      Dm.LedCtrl(GREEN_LED, ON);
      vTaskDelay(500);
      Dm.LedCtrl(GREEN_LED, OFF);
      vTaskDelay(500);
    }
  }

  vTaskDelete(NULL);
}

void setup()
{
  DEBUGPRNT.begin(115200);

  vSetErrorSerial(&DEBUGPRNT);

  xTaskCreate(main_task, "main_task", 1024, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error scheduler failed to start
  // should never get here
  while (1)
  {
    DEBUGPRNT.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
  // put your main code here, to run repeatedly:
}